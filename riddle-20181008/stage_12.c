#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/mman.h>

/*
 * Given a file and a char
 * open the file and mmap
 * it to the memory and write
 * at c[111]
 */
int main(int argc, char **argv)
{

        int fd = open(argv[1], O_RDWR);
        char *mapping = mmap(NULL,
                             4096,
                             PROT_READ|PROT_WRITE,
                             MAP_SHARED,
                             fd, 0);
        mapping[111] = argv[2][0];

        return 0;
}
