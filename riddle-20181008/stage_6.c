#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/wait.h>
/*
 * Master program to run riddle after duplicating fd 99
 */
int main(int argc, char **argv)
{
    // char *myfifo = "./stage_6_fifo";
    // char *yourfifo = "./stage_6_fifo2";
    // // Create - Open an named pipe with name stage_5
    // mkfifo(myfifo, 0666);
    // mkfifo(yourfifo, 0666);
    //
    // int fd1 = open(myfifo, O_RDWR);
    // // int fd2 = open(myfifo, O_WRONLY);
    // int fd3 = open(yourfifo, O_RDWR);
    // // int fd4 = open(yourfifo, O_WRONLY);

    int pipefd2[2];
    int pipefd1[2];
    if (pipe(pipefd2) < 0) {
        perror("pipe2");
        return -1;
    }
    if (pipe(pipefd1) < 0) {
        perror("pipe1");
        return -1;
    }

    // Duplicate the fd to fd 99 that riddle wants
    dup2(pipefd2[0], 33); // 2nd child reads from 2nd fifo
    dup2(pipefd2[1], 34); // 1st child writes to 2nd fifo
    dup2(pipefd1[0], 53); // 1st child reads from 1st fifo
    dup2(pipefd1[1], 54); // 2nd child writes to 1st fifo

    printf("forking()");
    pid_t pid = fork();

    if (pid < 0) {
        /* fork failed */
        perror("fork");
        return 127;
    }
    else if (pid == 0) {
        /* CHILD */

        if (execve("./riddle", NULL, NULL)) { perror("./riddle"); return 127;};

        /* Unreachable */
    }

    /* PARENT */
    // wait for pid to change state, dont care about the status=NULL and no flags=0
    waitpid(pid, NULL, 0);

    printf("Terminating\n");
    return 0;
}
