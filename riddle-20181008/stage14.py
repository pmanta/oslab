import os

WANTED_PID = 32767

def child():
    #print('\nA new child ',  os.getpid())
    mypid = os.getpid();
    if mypid != WANTED_PID:
        os._exit(0);
    else:
        os.execv("./riddle", ["./riddle"]);

def parent():
   while True:
      newpid = os.fork();
      if newpid == 0:
         child();
      else:
         pids = (os.getpid(), newpid);
         print("parent: %d, child: %d\n" % pids)

         if newpid != WANTED_PID:
             print "Forking again\n";
             continue;
         else:
            print "Exiting\n";
            break;


parent()
