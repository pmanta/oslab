#include <unistd.h>
#include <fcntl.h>

/*
 * Master program to run riddle after duplicating fd 99
 */
int main(int argc, char **argv)
{
    // Open a file named state_5.txt if it doesn't exists create it
    int fd1 = open("./stage_5.txt", O_CREAT | O_RDWR, 0666);

    // Duplicate the fd to fd 99 that riddle wants
    dup2(fd1, 99);

    // fork the riddle so it gets a copy of fd 99 and succed
    execve("./riddle", NULL, NULL);

    return 0;
}
