/*
 * lunix-chrdev.c
 *
 * Implementation of character devices
 * for Lunix:TNG
 *
 * Panagiotis Mantafounis <panagiotismantafounis@gmail.com>
 *
 */

#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mmzone.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>

#include "lunix.h"
#include "lunix-chrdev.h"
#include "lunix-lookup.h"

/*
 * Global data
 */
struct cdev lunix_chrdev_cdev;

/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	unsigned long flags;

	WARN_ON ( !(sensor = state->sensor));
	/*
	 *	Here we check the timestamp of the state
	 *  with the "last_update" of the sensor
	 *  and if the last update is different from
	 *  the timestamp we update the state with the new msrs.
	 */

	spin_lock_irqsave(&sensor->lock, flags);
	uint32_t ts = sensor->msr_data[state->type]->last_update;
	spin_unlock_irqrestore(&sensor->lock, flags);

	return ts > state->buf_timestamp;
}

/* convert the value and store it to the state buffer to send to the user */
static int lunix_chrdev_buffer_update(unsigned char *buf, long value)
{
	debug("We are in value = %d!!\n", value);
	unsigned char *tmp = buf;
	int ret = 0;
	int dec = 1;
	int i;
	if (value < 0) {
		*(tmp++) = '-';
		ret++;
		value = -value;
	}

	// 23666
	// Find how many digits are there
	while (value / dec > 0) dec *= 10;

	dec /= 10;

	// Check if the number is lower than 1000
	if (dec < 1000) {
		*(tmp++) = '0';
		*(tmp++) = '.';
		ret += 2;
	}

	debug("buf = %s, dec = %d\n", buf, dec);
	for (i = dec; i > 0; i/=10) {
		*(tmp++) = '0' + value / i;
		ret++;
		if (i == 1000) {
			*(tmp++) = '.';
			ret++;
		}
		value = value % i;
	}
	*tmp = '\n';
	ret++;

	debug("buf = %s, ret = %d\n", buf, ret);
	return ret;
}

/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state)
{
	struct lunix_sensor_struct *sensor;
	sensor = state->sensor;

	// used to store the state of the interrupts (enabled, disabled)
	unsigned long flags;

	/*
	 * Grab the raw data quickly, hold the
	 * spinlock for as little as possible.
	 */
	/* ? */
	/* Why use spinlocks? See LDD3, p. 119 */
	/* spin_lock_irq/_irqsave */
	/* gia tin anafora */
	spin_lock_irqsave(&sensor->lock, flags);
	uint32_t val = sensor->msr_data[state->type]->values[0];
	uint32_t ts = sensor->msr_data[state->type]->last_update;
	spin_unlock_irqrestore(&sensor->lock, flags);


	/*
	 * Any new data available?
	 * load them to the buffers
	 */
	if (state->buf_timestamp == ts) {
		debug("No new data %d == %d \n", state->buf_timestamp, ts);
		return -EAGAIN;
	} else {
		// we are in a locking state now
		debug("New data came at %d with value %d\n", ts, val);
		debug("%d != %d\n", state->buf_timestamp, ts);
		state->buf_timestamp = ts;

		// Load them to the buffers
		debug("Work to put them to buffers\n");
		switch (state->type) {
			case BATT:
				debug("Its a battery sensor\n");
				state->buf_lim = lunix_chrdev_buffer_update(state->buf_data, lookup_voltage[val]);
				break;
			case TEMP:
				debug("Its a temperature sensor\n");
				state->buf_lim = lunix_chrdev_buffer_update(state->buf_data, lookup_temperature[val]);
				break;
			case LIGHT:
				debug("Its a light sensor\n");
				state->buf_lim = lunix_chrdev_buffer_update(state->buf_data, lookup_light[val]);
				break;
		}

		return 0;
	}
}

/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp)
{
	/* Declarations */
	/* ? */
	struct lunix_chrdev_state_struct *state;
	unsigned int minor = iminor(inode);
	int sensor_number = minor / 8;

	int ret;

	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto out;

	/*
	 * Associate this open file with the relevant sensor based on
	 * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
	 */

	 /*
	  * get the sensor corresponding to this minor number
	  * Some info:
	  *	lunix-module.c allocates the sensors
	  * each sensor has a *msr_data[] array
	  * so I will have to assosiate the state->sensor
	  * and after that when we read I will have to take the
	  * msr_data[] depending on the minor number of the device
	  * and give it to the user?
	  */

	state = kmalloc(sizeof(struct lunix_chrdev_state_struct *), GFP_KERNEL);

	if (!state){
		ret = -1;
		goto out;
	}

	state->sensor = &lunix_sensors[sensor_number];
	//state->sensor = lunix_sensors;
	state->type = minor % 8;
	sema_init(&(state->lock), 1);
	state->buf_timestamp = 0;
	/* Allocate a new Lunix character device private state structure */
	filp->private_data = state;
out:
	return ret;
}

static int lunix_chrdev_release(struct inode *inode, struct file *filp)
{
	/*
	 * Release the memory allocated to state
	 */
	struct lunix_chrdev_state_struct *state;
	state = filp->private_data;

	if (state)
		kfree(state);

	debug("Char device released!\n");
	return 0;
}

static long lunix_chrdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Why? */
	return -EINVAL;
}

static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos)
{
	ssize_t ret;

	struct lunix_sensor_struct *sensor;
	struct lunix_chrdev_state_struct *state;
	int data_to_write, difference;

	state = filp->private_data;
	WARN_ON(!state);

	sensor = state->sensor;
	WARN_ON(!sensor);

	/* Lock? */
	/* Check if the lock is taken */
	if (down_interruptible(&state->lock)) {
		ret = -ERESTARTSYS;
		goto out;
	}
	/*
	 * If the cached character device state needs to be
	 * updated by actual sensor data (i.e. we need to report
	 * on a "fresh" measurement, do so
	 */
	if (*f_pos == 0) {
		while (lunix_chrdev_state_update(state) == -EAGAIN) {

			// unlock the semaphore before sleeping the process
			up(&state->lock);

			// if the process is non blocked
			if (filp->f_flags & O_NONBLOCK)
				return -EAGAIN;

			/* The process needs to sleep */
			/* See LDD3, page 153 for a hint */
			// lock the semaphore
			if (wait_event_interruptible(sensor->wq,
			        lunix_chrdev_state_needs_refresh(state))) {
				return -ERESTARTSYS;
			}


			if (down_interruptible(&state->lock)) {
				ret = -ERESTARTSYS;
				goto out;
			}
		}
	}

	/* Determine the number of cached bytes to copy to userspace */
	if (*f_pos + cnt > state->buf_lim)
		data_to_write = state->buf_lim - *f_pos;
	else
		data_to_write = cnt;

	debug("Copying to user %s That has the size of %d\n",
	      state->buf_data + *f_pos,
		  data_to_write);

	if (copy_to_user(usrbuf, state->buf_data + *f_pos, data_to_write)) {
		debug("Fault in copy to user\n");
		return -EFAULT;
	}
	ret = data_to_write;
	*f_pos = *f_pos + data_to_write;

	/* Auto-rewind on EOF mode? */
	if (*f_pos >= state->buf_lim)
		*f_pos = 0;
out:
	/* Unlock? */
	up(&state->lock);
	return ret;
}

static int lunix_chrdev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	return -EINVAL;
}

static struct file_operations lunix_chrdev_fops =
{
        .owner          = THIS_MODULE,
	.open           = lunix_chrdev_open,
	.release        = lunix_chrdev_release,
	.read           = lunix_chrdev_read,
	.unlocked_ioctl = lunix_chrdev_ioctl,
	.mmap           = lunix_chrdev_mmap
};

int lunix_chrdev_init(void)
{
	/*
	 * Register the character device with the kernel, asking for
	 * a range of minor numbers (number of sensors * 8 measurements / sensor)
	 * beginning with LINUX_CHRDEV_MAJOR:0
	 */
	int ret;
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

	debug("initializing character device\n");
	cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
	lunix_chrdev_cdev.owner = THIS_MODULE;
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	/* ? */
	// how many char devices we need?
	ret = register_chrdev_region(dev_no, lunix_minor_cnt, "Lunix_chrdev");
	/* register_chrdev_region? */
	if (ret < 0) {
		debug("failed to register region, ret = %d\n", ret);
		goto out;
	}
	/* ? */
	/* cdev_add? */
	ret = cdev_add(&lunix_chrdev_cdev, dev_no, lunix_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device\n");
		goto out_with_chrdev_region;
	}
	debug("completed successfully\n");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
	return ret;
}

void lunix_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

	debug("entering\n");
	dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
	cdev_del(&lunix_chrdev_cdev);
	unregister_chrdev_region(dev_no, lunix_minor_cnt);
	debug("leaving\n");
}
