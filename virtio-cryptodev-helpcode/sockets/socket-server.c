/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Panagiotis Mantafounis <panagiotismantafounis@gmail.com>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#define MAX_CLIENTS 3

/* Convert a buffer to upercase */
void toupper_buf(char *buf, size_t n)
{
	size_t i;

	for (i = 0; i < n; i++)
		buf[i] = toupper(buf[i]);
}

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

/* Insist until all of the data has been read */
ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

void init_array(int *arr, int size)
{
	int i;
	for (i = 0; i < size; i++)
		arr[i] = 0;
}

/* Add master socket and client sockets
 * that are active to the fd_set
 * and return the maximum fd */
int add_to_set(int *arr, int size, int master_socket, fd_set *set)
{
	int max_sd = master_socket;
	int newsd;
	FD_SET(master_socket, set);
	int i;
	for (i = 0; i < size; i++) {
		// For each client check if there is a valid sd if there is add it to select
		newsd = arr[i];

		if (newsd > 0)
			FD_SET(newsd, set);

		if (max_sd < newsd)
			max_sd = newsd;
	}

	return max_sd;
}

/* Find an empty place to add newsd to the list of fds */
void add_sd_to_clients (int *arr, int size, int newsd)
{
	int i;
	for (i = 0; i < size; i++) {
		// Store the sd in the first empty sd you find
		if (arr[i] == 0) {
			arr[i] = newsd;
			break;
		}
	}
}
/* Write to everyone that is connected */
void write_to_everyone(int *arr, int size, char *buf, ssize_t n, int dont_sent)
{
	int newsd;
	int i;
	for (i = 0; i < size; i++) {
		newsd = arr[i];

		if (newsd == 0 || newsd == dont_sent) continue;

		/* newsd is active */
		if (insist_write(newsd, buf, n) != n) {
			perror("write to remote peer failed");
			break;
		}

		fprintf(stdout, "write : success\n");
	}
}

/* Find the sd that cause select to activate */
void handle_sds(int *arr, int size, fd_set *set)
{
	int newsd;
	char buf[96];
	ssize_t n;
	int i;
	for (i = 0; i < size; i++) {
		newsd = arr[i];

		if (FD_ISSET(newsd, set)) {
			// Read
			n = insist_read(newsd, buf, sizeof(buf));
			if (n <= 0) {
				if (n < 0)
					perror("read from remote peer failed");
				else {
					fprintf(stderr, "Peer went away clearing its socket\n");
					close(newsd);
					arr[i] = 0;
				}
			} else {
				fprintf(stderr, "Got %ld bytes\n", n);
				/* Info taken write it to everyone except the one that sent */
				write_to_everyone(arr, size, buf, n, newsd);
			}
		}
	}
}



int main(void)
{
	char addrstr[INET_ADDRSTRLEN];
	int sd, newsd;
	int max_sd;
	int client_sds[MAX_CLIENTS];
	// ssize_t n;
	socklen_t len;
	struct sockaddr_in sa;
	int retval;

	fd_set rfds;
	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	init_array(client_sds, MAX_CLIENTS);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}

	/* Loop forever, accept()ing connections */
	for (;;) {
		fprintf(stderr, "Waiting for an incoming connection...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		// Use select for everything add to the fd set all the client sds
		// and the master sd that listens for connections

		FD_ZERO(&rfds);

		// Add the master socket (server socket which is listening for connetions to select)
		// and add every client sd that is active
		max_sd = add_to_set(client_sds, MAX_CLIENTS, sd, &rfds);

		// Wait with select for any activity in the fds/sds
		retval = select(max_sd + 1, &rfds, NULL, NULL, NULL);

		// After select returns we have an activity in one of the descriptors
		// Find out which one
		if (retval == -1) {
        		perror("select()");
			exit(1);
		}

		// If is the master socket (server) then its an incomming connection
		if (FD_ISSET(sd, &rfds)) {
			// Accept the connection and add the sd to the client sds
			if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
				perror("accept");
				exit(1);
			}
			if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
				perror("could not format IP address");
				exit(1);
			}
			fprintf(stderr, "Incoming connection from %s:%d\n",
				addrstr, ntohs(sa.sin_port));


			/* Find somewhere to add the new sd connector */
			add_sd_to_clients(client_sds, MAX_CLIENTS, newsd);
		}

		// ELSE its some other i/o on an already added sd

		// Find out which client is
		// then read what it has to say
		// Write what it has to say to every
		// connected client
		handle_sds(client_sds, MAX_CLIENTS, &rfds);
	}

	/* This will never happen */
	return 1;
}
