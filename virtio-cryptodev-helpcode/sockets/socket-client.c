/*
 * socket-client.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Panagiotis Mantafounis <panagiotismantafounis@gmail.com>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#include <crypto/cryptodev.h>
#include <sys/ioctl.h>

#define DATA_SIZE 96
#define KEY_SIZE 16
#define BLOCK_SIZE 16

unsigned char KEY[16] = "abcdefghigklmno";
unsigned char IV[16] = "higklmnoabcdefg";

struct data
{
	unsigned char in[DATA_SIZE],
		      encrypted[DATA_SIZE],
		      decrypted[DATA_SIZE],
		      key[KEY_SIZE],
		      iv[BLOCK_SIZE]; //+1 for the null terminator; //+1 for the null terminator
};


/* Insist until all of the data has been read */
ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;

	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

int encrypt(int cfd, unsigned char *message, unsigned char *encrypted, int size)
{
	struct session_op sess;
	struct crypt_op cryp;
	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = KEY;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len = size;
	cryp.src = message;
	cryp.dst = encrypted;
	cryp.iv = IV;
	cryp.op = COP_ENCRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}

	/* Finish crypto session */
	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
		return 1;
	}

	return 0;
}

int decrypt(int cfd, unsigned char *message, unsigned char *decrypted, int size)
{
	struct session_op sess;
	struct crypt_op cryp;

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));

	/*
	 * Get crypto session for AES128
	 */
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = KEY;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}

	/*
	 * Encrypt data.in to data.encrypted
	 */
	cryp.ses = sess.ses;
	cryp.len = size;
	cryp.src = message;
	cryp.dst = decrypted;
	cryp.iv = IV;
	cryp.op = COP_DECRYPT;

	if (ioctl(cfd, CIOCCRYPT, &cryp)) {
		perror("ioctl(CIOCCRYPT)");
		return 1;
	}

	/* Finish crypto session */
	if (ioctl(cfd, CIOCFSESSION, &sess.ses)) {
		perror("ioctl(CIOCFSESSION)");
		return 1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int fd;



	int sd, port;
	ssize_t n;
	unsigned char buf[DATA_SIZE];
	unsigned char encrypted[DATA_SIZE];
	unsigned char decrypted[DATA_SIZE];
	memset(buf, 0, sizeof(buf)); // clear the buffer
	memset(encrypted, 0, sizeof(encrypted)); // clear the buffer
	memset(decrypted, 0, sizeof(decrypted)); // clear the buffer
	char *hostname;
	char *username;
	struct hostent *hp;
	struct sockaddr_in sa;
	int retval;
	fd_set rfds;

	if (argc != 4) {
		fprintf(stderr, "Usage: %s hostname port username\n", argv[0]);
		exit(1);
	}
	hostname = argv[1];
	port = atoi(argv[2]); /* Needs better error checking */
	username = argv[3];

	fprintf(stderr, "username = %s\n", username);
	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... ");
	fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");

	fd = open("/dev/cryptodev0", O_RDWR);
	if (fd < 0) {
		perror("open(/dev/cryptodev0)");
		return 1;
	}

	fprintf(stderr, "Opened crypto device\n");

	// Here we are connected to the server
	// now add 2 fds to select 1 for the socket
	// and 1 for the stdin.
	for (;;) {
		/* Add the prefix of the username to the message */
		strncpy((char *) buf, username, strlen(username));
		strncpy((char *) buf + strlen(username), ": ", 2);

		FD_ZERO(&rfds);

		// STDIN
		FD_SET(STDIN_FILENO, &rfds);
		FD_SET(sd, &rfds);

		retval = select(sd + 1, &rfds, NULL, NULL, NULL);

		if (retval == -1) {
        		perror("select()");
			exit(1);
		}
		// If it is the stdin then read the data and send it to the socket
		if (FD_ISSET(STDIN_FILENO, &rfds)) {
			int ret = read(STDIN_FILENO, buf + strlen(username) + 2, sizeof(buf));
			buf[sizeof(buf)-1] = '\0';

			if (ret < 0) {
				perror("read");
				exit(1);
			}

			// fprintf(stderr, "Unencrypted:\n");
			// for (int i = 0; i < DATA_SIZE; i++)
			// 	fprintf(stderr, "%x", buf[i]);
			//
			// fprintf(stderr, "\n");

			// Encrypt the message using the cryptodev device
			if (encrypt(fd, buf, encrypted, DATA_SIZE) < 0) {
				return 1;
			}

			// fprintf(stderr, "Encrypted:\n");
			// for (int i = 0; i < DATA_SIZE; i++)
			// 	fprintf(stderr, "%x", encrypted[i]);
			//
			// fprintf(stderr, "\n");

			// Write to the socket
			if (insist_write(sd, encrypted, sizeof(encrypted)) != sizeof(encrypted)) {
				perror("write");
				exit(1);
			}

			memset(encrypted, 0, sizeof(encrypted));
		}

		// if its the socket read the data and print them to stdout
		if (FD_ISSET(sd, &rfds)) {

			n = insist_read(sd, buf, sizeof(buf)); // Read from the server

			if (n < 0) {
				perror("read");
				exit(1);
			}

			if (n <= 0) {
				fprintf(stderr, "Break\n");
				break;
			}

			/* Decrypt the message */
			if (decrypt(fd, buf, decrypted, DATA_SIZE) < 0) {
				return 1;
			}

			// fprintf(stderr, "%d\n", strlen(decrypted));
			// for (int i = 0; i < DATA_SIZE; i++)
			// 	fprintf(stderr, "%x", decrypted[i]);
			//
			// fprintf(stderr, "\n");

			if (insist_write(0, decrypted, DATA_SIZE) != DATA_SIZE) {
				perror("write");
				exit(1);
			}

			memset(decrypted, 0, sizeof(decrypted));
		}

		// Empty the buffer
		memset(buf, 0, sizeof(buf));
	}

	close(fd);
	fprintf(stderr, "\nDone.\n");
	return 0;
}
