/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-cryptodev device
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 * Panagiotis Mantafounis <panagiotismantafounis@gmail.com>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "cryptodev.h"

/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	int num_in = 0, num_out = 0;
	unsigned long flags;
	unsigned int len;
	struct crypto_open_file *crof;
	struct crypto_device *crdev;
	unsigned int *syscall_type;
	unsigned int *host_fd;


	/* Declare the sg lists */
	struct scatterlist host_fd_sg, syscall_type_sg, *sgs[2];
	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_OPEN;
	host_fd = kzalloc(sizeof(*host_fd), GFP_KERNEL);
	*host_fd = -1;

	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor",
		      iminor(inode));
		ret = -ENODEV;
		goto fail;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail;
	}
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;

	/**
	 * We need two sg lists, one for syscall_type and one to get the
	 * file descriptor from the host.
	 **/
	/* ?? */
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;

	/* input scatter list */
	sg_init_one(&host_fd_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[num_out + num_in++] = &host_fd_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* Add the sgs in the vq */

	/* Lock the spinlock */
	// Diaforetiko lock gia crypto_device
	// spin_lock_irqsave(&crdrvdata.lock, flags);

	if (down_interruptible(&crdev->lock)) {
		ret = -ERESTARTSYS;
		goto fail;
	};

	/* add scatter lists to virtqueue */
	err = virtqueue_add_sgs(crdev->vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	/* Notify the backend */
	virtqueue_kick(crdev->vq);

	/* Wait for the host to process our data */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL);

	// spin_unlock_irqrestore(&crdrvdata.lock, flags);
	up(&crdev->lock);

	/* If host failed to open() return -ENODEV. */
	/* ?? */
	if (crof->host_fd <= 0) {
		debug("Host failed to open the file\n");
		ret = -ENODEV;
		goto fail;
	}


fail:
	kfree(syscall_type);
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int *syscall_type;
	int num_out = 0, num_in = 0;
	unsigned int len;
	unsigned long flags;

	/* Declare the sg lists */
	struct scatterlist host_fd_sg, syscall_type_sg, ret_sg, *sgs[3];

	debug("Entering");

	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_CLOSE;

	/**
	 * Send data to the host.
	 **/
	/* ?? */
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;

	/* out host fd scatter list */
	sg_init_one(&host_fd_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[num_out++] = &host_fd_sg;

	/* input the return value of the close */
	sg_init_one(&ret_sg, &ret, sizeof(ret));
	sgs[num_out + num_in++] = &ret_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* Lock the spinlock */
	if (down_interruptible(&crdev->lock)) {
		ret = -ERESTARTSYS;
		goto fail;
	};
	/* add scatter lists to virtqueue */
	err = virtqueue_add_sgs(crdev->vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	/* Notify the backend */
	virtqueue_kick(crdev->vq);

	/* Wait for the host to process our data */
	while (virtqueue_get_buf(crdev->vq, &len) == NULL);

	up(&crdev->lock);

fail:
	kfree(syscall_type);
	kfree(crof);
	debug("Leaving");
	return ret;

}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd,
                                unsigned long arg)
{
	struct crypto_data {
		struct session_op sop;
		struct crypt_op crypop;
		__u8  *crypt_src;
		__u8  *crypt_dest;
		__u8  *crypt_iv;
		unsigned char *key;
	} *crdata;


	struct session_op *sop;
	struct crypt_op *crypop;

	unsigned long flags;
	int *ret;
	int err;

	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	// struct scatterlist syscall_type_sg, host_fd_sg, ret_sg,
	//                    output_msg_sg, input_msg_sg, cmd_sg,
	//                    *sgs[6];
	struct scatterlist syscall_type_sg, host_fd_sg, ret_sg, ses_key_sg,
	                   output_msg_sg, input_msg_sg, cmd_sg, sop_sg, ses_sg,
	                   crypop_sg, src_sg, dest_sg, iv_sg, *sgs[10];
	unsigned int num_out, num_in, len;
#define MSG_LEN 100
	unsigned char *output_msg, *input_msg;
	unsigned int *syscall_type;
	unsigned int *syscall_cmd;
	debug("Entering");
	debug("Calling ioctl with cmd = %d, arg = %ld\n", cmd, arg);
	/**
	 * Allocate all data that will be sent to the host.
	 **/
	crdata = kzalloc(sizeof(struct crypto_data), GFP_KERNEL);
	ret = kzalloc(sizeof(int), GFP_KERNEL);
	syscall_cmd = kzalloc(sizeof(*syscall_cmd), GFP_KERNEL);
	*syscall_cmd = cmd;
	output_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	input_msg = kzalloc(MSG_LEN, GFP_KERNEL);
	syscall_type = kzalloc(sizeof(*syscall_type), GFP_KERNEL);
	*syscall_type = VIRTIO_CRYPTODEV_SYSCALL_IOCTL;

	*ret = 0;
	num_out = 0;
	num_in = 0;

	/**
	 *  These are common to all ioctl commands.
	 **/
	sg_init_one(&syscall_type_sg, syscall_type, sizeof(*syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	/* ?? */

	/* out host fd scatter list */
	sg_init_one(&host_fd_sg, &crof->host_fd, sizeof(crof->host_fd));
	sgs[num_out++] = &host_fd_sg;

	/* out cmd */
	sg_init_one(&cmd_sg, syscall_cmd, sizeof(cmd));
	sgs[num_out++] = &cmd_sg;

	/**
	 *  Add all the cmd specific sg lists.
	 **/
	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		sop = (struct session_op *) arg;
		if (unlikely(copy_from_user(&crdata->sop, sop, sizeof(struct session_op))))
			return -EFAULT;

		// Crdata-> key na perasw to pointer tou key kai meta copy apo auto pou mou epistrefei to backend
		crdata->key = kzalloc(crdata->sop.keylen * sizeof(__u8), GFP_KERNEL);
		if (!crdata->key)
	   		return -ENOMEM;


		if(unlikely(copy_from_user(crdata->key, crdata->sop.key, crdata->sop.keylen * sizeof(__u8))))
			return -EFAULT;

		debug("ses_key = %s\n", crdata->key);

		/* dereference the sop.key */
		/* Pass the sop to the sgs and the CIOCSESSION CMD */
		memcpy(output_msg, "Hello HOST from ioctl CIOCGSESSION.", 36);
		input_msg[0] = '\0';
		/* outputs */
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		sg_init_one(&ses_key_sg, crdata->key, crdata->sop.keylen * sizeof(__u8));
		sgs[num_out++] = &ses_key_sg;

		/* inputs */
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		sg_init_one(&sop_sg, &crdata->sop, sizeof(struct session_op));
		sgs[num_out + num_in++] = &sop_sg;
		sg_init_one(&ret_sg, ret, sizeof(ret));
		sgs[num_out + num_in++] = &ret_sg;

		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION");
		/* get the ses pointer from the user */
		if (unlikely(copy_from_user(&crdata->sop.ses, (__u32 *) arg, sizeof(__u32))))
			return -EFAULT;

		memcpy(output_msg, "Hello HOST from ioctl CIOCFSESSION.", 36);
		input_msg[0] = '\0';
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;
		sg_init_one(&ses_sg, &crdata->sop.ses, sizeof(__u32));
		sgs[num_out++] = &ses_sg;
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;
		sg_init_one(&ret_sg, ret, sizeof(ret));
		sgs[num_out + num_in++] = &ret_sg;
		break;

	case CIOCCRYPT:
		debug("CIOCCRYPT");
		crypop = (struct crypt_op *) arg;

		/* Read crypt_op values */
		if (unlikely(copy_from_user(&crdata->crypop,
			                    crypop,
			                    sizeof(struct crypt_op))))
			return -EFAULT;


		debug("Read from user .ses = %d, .len = %d, .op = %d\n",
		      crdata->crypop.ses, crdata->crypop.len, crdata->crypop.op);

		/* Read src data */
      		crdata->crypt_src = kzalloc(crdata->crypop.len * sizeof(__u8),
      		                             GFP_KERNEL);
      		if (!crdata->crypt_src)
      			return -ENOMEM;

      		if(unlikely(copy_from_user(crdata->crypt_src,
      			                   (__u8 __user *) crypop->src,
      					   crdata->crypop.len * sizeof(__u8))))
      			return -EFAULT;


		debug("Read src from user .src = %s\n", crdata->crypt_src);

		crdata->crypt_iv = kzalloc(VIRTIO_CRYPTODEV_BLOCK_SIZE * sizeof(__u8),
      		                             GFP_KERNEL);

		if(unlikely(copy_from_user(crdata->crypt_iv,
      			                   (__u8 __user *) crypop->iv,
      					   VIRTIO_CRYPTODEV_BLOCK_SIZE * sizeof(__u8))))
      			return -EFAULT;

		/* Create a pointer to pass for getting the destination data */
		crdata->crypt_dest = kzalloc(crdata->crypop.len * sizeof(__u8),
      		                             GFP_KERNEL);

		if(unlikely(copy_from_user(crdata->crypt_dest,
	   			           (__u8 __user *) crypop->dst,
	   			 	   crypop->len * sizeof(__u8))))
	   			return -EFAULT;

		/* Construct the scatter lists to transfer to the backend */

		memcpy(output_msg, "Hello HOST from ioctl CIOCCRYPT.", 33);
		input_msg[0] = '\0';
		sg_init_one(&output_msg_sg, output_msg, MSG_LEN);
		sgs[num_out++] = &output_msg_sg;

		/* src data */
		sg_init_one(&src_sg, crdata->crypt_src, crdata->crypop.len * sizeof(__u8));
		sgs[num_out++] = &src_sg;
		/* iv data */
		sg_init_one(&iv_sg, crdata->crypt_iv, VIRTIO_CRYPTODEV_BLOCK_SIZE * sizeof(__u8));
		sgs[num_out++] = &iv_sg;

		/* INPUTS */
		sg_init_one(&input_msg_sg, input_msg, MSG_LEN);
		sgs[num_out + num_in++] = &input_msg_sg;

		/* crypop struct*/
		sg_init_one(&crypop_sg, &crdata->crypop, sizeof(struct crypt_op));
		sgs[num_out + num_in++] = &crypop_sg;

		sg_init_one(&dest_sg, crdata->crypt_dest, crdata->crypop.len * sizeof(__u8));
		sgs[num_out + num_in++] = &dest_sg;

		sg_init_one(&ret_sg, ret, sizeof(ret));
		sgs[num_out + num_in++] = &ret_sg;

		break;

	default:
		debug("Unsupported ioctl command");
		break;
	}


	/**
	 * Wait for the host to process our data.
	 **/
	/* ?? */
	/* ?? Lock ?? */
	debug("pushing vq!!");
	if (down_interruptible(&crdev->lock)) {
		*ret = -ERESTARTSYS;
		goto fail;
	};
	err = virtqueue_add_sgs(vq, sgs, num_out, num_in,
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(vq);
	while (virtqueue_get_buf(vq, &len) == NULL)
		/* do nothing */;

	up(&crdev->lock);
	/* Return the correct responses to the user */
	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION return to user");
		sop = (struct session_op *) arg;
 		if (*ret == 0) {
			debug("Return success with ses = %d\n", crdata->sop.ses);
			// Write retrieved sess.ses to the user sess.ses
			if (unlikely(copy_to_user(&sop->ses, &crdata->sop.ses, sizeof(__u32))))
				return -EFAULT;
		}
		break;

	case CIOCFSESSION:
		debug("CIOCFSESSION return to user");
		break;

	case CIOCCRYPT:
		debug("CIOCCRYPT return");
		crypop = (struct crypt_op *) arg;
		debug("Got data %s\n", crdata->crypt_dest);
		/* Write the result taken by the sgs to the user */
		if (*ret == 0) {
			debug("Return success\n");
			// Write the destination data to the destination in user space
			if (unlikely(copy_to_user(crypop->dst, crdata->crypt_dest,  crdata->crypop.len * sizeof(__u8))))
				return -EFAULT;
		}
		break;

	default:
		debug("Unsupported ioctl command");
		break;
	}
	debug("We said: '%s'", output_msg);
	debug("Host answered: '%s'", input_msg);

fail:
	if (crdata)
	    {
	        if (crdata->key)
	            kfree(crdata->key);
	        if (crdata->crypt_dest)
	            kfree(crdata->crypt_dest);
	        if (crdata->crypt_src)
	            kfree(crdata->crypt_src);
		if (crdata->crypt_iv)
    	            kfree(crdata->crypt_iv);
	        kfree(crdata);
	    }


	kfree(output_msg);
	kfree(input_msg);
	kfree(syscall_type);
	kfree(syscall_cmd);
	debug("Leaving");

	return *ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf,
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops =
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("Initializing character device...");
	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;

	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}

	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
